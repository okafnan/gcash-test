# GCash Java Practical Exam For Java Developer

The following repo contains only code base.

## Introduction

This is test for the position of *Java Developer* in **GCash**.
I completely performed the test as per provided instruction in the test document:

I developed following steps:

1. I created Restful API services for **Calculate The Shipping Cost** base on requirement. 
2. I called external service API of **Voucher API** with help of Swagger-hub mock server.
3. Extracted data from Voucher API and provide the discount on the bases of voucher code.

## Getting Started

###  Java Practical Exam

In this project I developed three major parts.

* Developed Rest API for calculate the shipping cost
* Call to Third Party Voucher API
* Provide Discount on behalf of Voucher Code.

### Calculate Shipping Cost (Rest API) 

Calculate the shipping cost using provided rule names and its cost. I used POST method to get data using rest API. 

This is example of rest API for calculate shipping cost

```json
URL: http://localhost:8081/shippingCost/calculate
Body: 
{
    "weight":"8",
    "height":"10",
    "width": "7",
    "length":"5",
    "voucherCode": "GFI",
    "key": "apikey"
}
Response:
{
    "costOfShipment": "10.5 PHP",
    "message": "Volume is less than 1500 cm3 For Small Parcel, Your Shipping Cost is: 10.5 PHP",
    "code": "GFI",
    "discount": "7.5",
    "expiry": "2020-09-16",
    "voucherMessage": "Your voucher code has been expired on this date 2020-09-16 ... ",
    "error": null,
    "vouchorExpired": true
}


```

_____

### Call External Service (Voucher API)

Shipping Cost API called the external API for getting discount the bases of **Voucher Code**  and **KEY**

This is example of testing voucher API:
```json
URL: http://https://mynt-exam.mocklab.io/voucher/MYNT?key=apikey

Response:
{
    "code": "MYNT",
    "discount": 12.25,
    "expiry": "2020-09-16"
}
```
