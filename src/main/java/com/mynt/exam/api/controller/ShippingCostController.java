package com.mynt.exam.api.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mynt.exam.api.model.InputParam;
import com.mynt.exam.api.response.ShippingCostResponse;
import com.mynt.exam.api.service.ShippingCostCalculator;

//This is EndPoint of this API 
//localhost:8081/shippingCost/calculate

//Example Parameters
/*{
    "weight":"58",
    "height":"100",
    "width": "70",
    "length":"5",
    "voucherCode": "GFI",
    "key": "apikey"
}*/

@RestController
@RequestMapping("/shippingCost")
public class ShippingCostController {

	private final static Log log = LogFactory.getLog(ShippingCostController.class.getName());

	
	@Autowired
	private ShippingCostCalculator shippingCostCalculator;
	
	@PostMapping("/calculate")
	public ResponseEntity<ShippingCostResponse> calculateShippingCost(@RequestBody InputParam inputParameters) {
		log.info("Function Called: calculateShippingCost");
		ShippingCostResponse shippingCostResponse = shippingCostCalculator.calculateShippingCostService(inputParameters);
		return ResponseEntity.ok().body(shippingCostResponse);

	}
}
