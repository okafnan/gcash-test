package com.mynt.exam.api.response;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VoucherApiResponse {

	private String code;
	private double discount;
	private LocalDate expiry;
	private String error;
	private boolean isVouchorExpired;

}
