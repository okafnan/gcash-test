package com.mynt.exam.api.util;

public class RulesNameConstant {
	
	public static final String REJECT = "Reject";
	public static final String HEAVY_PARCEL = "Heavy Parcel";
	public static final String SMALL_PARCEL = "Small Parcel";
	public static final String MEDIUM_PARCEL = "Medium Parcel";
	public static final String LARGE_PARCEL = "Large Parcel";
	
}
