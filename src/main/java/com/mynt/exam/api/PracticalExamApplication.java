package com.mynt.exam.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication
public class PracticalExamApplication {	

	public static void main(String[] args) {
		SpringApplication.run(PracticalExamApplication.class, args);
		
		
	}

}
