package com.mynt.exam.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputParam {
	
	private double weight;
	private double height;
	private double width;
	private double length;
	private String voucherCode;
	private String key;

}
