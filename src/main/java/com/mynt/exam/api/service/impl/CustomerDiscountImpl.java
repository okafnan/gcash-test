package com.mynt.exam.api.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.mynt.exam.api.response.ShippingCostResponse;
import com.mynt.exam.api.service.CustomerDiscount;

@Service
public class CustomerDiscountImpl implements CustomerDiscount {

	private final static Log log = LogFactory.getLog(CustomerDiscountImpl.class.getName());

	@Override
	public ShippingCostResponse isCustomerAvailDiscount(ShippingCostResponse shippingCostResponse) {
		log.info("Function Called: isCustomerAvailDiscount");

		// if any voucher code will be invalid so return function and set required
		// values
		if (shippingCostResponse.getError() != null
				&& shippingCostResponse.getError().equalsIgnoreCase("Invalid code")) {
			shippingCostResponse.setVoucherMessage("Your voucher code is invalid, Please check again");
			return shippingCostResponse;
		}

//		if any voucher expired or not so this condition will execute
		else if (shippingCostResponse.isVouchorExpired()) {
			String voucherExpiryMessage = "Your voucher code has been expired on this date "
					+ shippingCostResponse.getExpiry() + " ....! ";
			shippingCostResponse.setVoucherMessage(voucherExpiryMessage);
			return shippingCostResponse;
			
		} else {
			double discountedShippingCost = shippingCostResponse.getCostOfShipment()
					- shippingCostResponse.getDiscount();
			shippingCostResponse.setCostOfShipment(discountedShippingCost);
			String voucherExpiryMessage = "You avail " + shippingCostResponse.getDiscount()
					+ " PHP discount on Cost of Shipping Now Total Cost is: " + shippingCostResponse.getCostOfShipment()
					+ " PHP ....! ";
			shippingCostResponse.setVoucherMessage(voucherExpiryMessage);
			return shippingCostResponse;
		}
		
	}

}
