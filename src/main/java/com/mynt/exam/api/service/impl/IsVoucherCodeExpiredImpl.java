package com.mynt.exam.api.service.impl;

import java.time.LocalDate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.mynt.exam.api.response.ShippingCostResponse;
import com.mynt.exam.api.response.VoucherApiResponse;
import com.mynt.exam.api.service.IsVoucherCodeExpired;

@Service
public class IsVoucherCodeExpiredImpl implements IsVoucherCodeExpired {
	
	
	private final static Log log = LogFactory.getLog(IsVoucherCodeExpiredImpl.class.getName());

	@Override
	public boolean isVoucherCodeExpiredOrNot(VoucherApiResponse voucherApiResponse) {
		log.info("Function Called: isVoucherCodeExpiredOrNot");

		LocalDate voucherDate = voucherApiResponse.getExpiry();
		LocalDate today = LocalDate.now();
		
		boolean isVoucherExpired = false; 

		if (voucherDate.isBefore(today)) {
			isVoucherExpired=true;
		} 

		return isVoucherExpired;
	}


}
