package com.mynt.exam.api.service;

import com.mynt.exam.api.model.InputParam;
import com.mynt.exam.api.response.ShippingCostResponse;
import com.mynt.exam.api.response.VoucherApiResponse;

@FunctionalInterface
public interface VoucherApiService {

	VoucherApiResponse getDataFromVoucherApiServie(InputParam inputParameters);

	

}
