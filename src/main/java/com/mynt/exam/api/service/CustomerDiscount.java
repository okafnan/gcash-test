package com.mynt.exam.api.service;

import com.mynt.exam.api.response.ShippingCostResponse;

@FunctionalInterface
public interface CustomerDiscount {

	ShippingCostResponse isCustomerAvailDiscount(ShippingCostResponse shippingCostResponse);

}
