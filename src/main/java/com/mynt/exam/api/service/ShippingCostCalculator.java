package com.mynt.exam.api.service;

import com.mynt.exam.api.model.InputParam;
import com.mynt.exam.api.response.ShippingCostResponse;

@FunctionalInterface
public interface ShippingCostCalculator {

	ShippingCostResponse calculateShippingCostService(InputParam inputParameters);

}
