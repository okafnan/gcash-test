package com.mynt.exam.api.service;

import com.mynt.exam.api.response.VoucherApiResponse;

@FunctionalInterface
public interface IsVoucherCodeExpired {

	boolean isVoucherCodeExpiredOrNot(VoucherApiResponse voucherApiResponse);

}
