package com.mynt.exam.api.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.mynt.exam.api.model.InputParam;
import com.mynt.exam.api.response.ShippingCostResponse;
import com.mynt.exam.api.response.VoucherApiResponse;
import com.mynt.exam.api.service.CustomerDiscount;
import com.mynt.exam.api.service.ShippingCostCalculator;
import com.mynt.exam.api.service.VoucherApiService;
import com.mynt.exam.api.util.RulesNameConstant;

@Service
public class ShippingCostCalculatorImpl implements ShippingCostCalculator {

	private final static Log log = LogFactory.getLog(ShippingCostCalculatorImpl.class.getName());

	@Autowired
	private Environment environment;

	@Autowired
	private VoucherApiService voucherApiService;

	@Autowired
	private CustomerDiscount customerDiscount;
	String responseMessage = null;

//	Calculate shipment cost as per weight and volumes
	@Override
	public ShippingCostResponse calculateShippingCostService(InputParam inputParameters) {
		log.info("Function Called: calculateShippingCostService");

		double costOfShipment;
		double shippingCharges;
		ShippingCostResponse shippingCostResponse = new ShippingCostResponse();

		String currencyUnit = environment.getProperty("currencyUnit");
		log.info("Currency Unit: " + currencyUnit);

		double volume = inputParameters.getLength() * inputParameters.getWidth() * inputParameters.getHeight();

		if (inputParameters.getWeight() > 50) {
			responseMessage = "Weight exceeds 50KG, We can't entertain more than 50KG";
			log.info(responseMessage);

		} else if (inputParameters.getWeight() >= 10) {
			shippingCharges = Double.parseDouble(environment.getProperty("heavyParcelCostPerKg"));
			costOfShipment = shippingCharges * inputParameters.getWeight();
			responseMessage = "Weight exceeds 10kg For " + RulesNameConstant.HEAVY_PARCEL + ", Your Shipping Cost is: "
					+ costOfShipment + " " + currencyUnit;
			shippingCostResponse.setCostOfShipment(costOfShipment);
			shippingCostResponse.setMessage(responseMessage);
			log.info(responseMessage);

		} else if (volume <= 1500) {
			shippingCharges = Double.parseDouble(environment.getProperty("smallParcelCostAsPerVolume"));
			costOfShipment = shippingCharges * volume;
			responseMessage = "Volume is less than 1500 cm3 For " + RulesNameConstant.SMALL_PARCEL
					+ ", Your Shipping Cost is: " + costOfShipment + " " + currencyUnit;
			shippingCostResponse.setCostOfShipment(costOfShipment);
			shippingCostResponse.setMessage(responseMessage);
			log.info(responseMessage);

		} else if (volume <= 2500) {
			shippingCharges = Double.parseDouble(environment.getProperty("mediumParcelCostAsPerVolume"));
			costOfShipment = shippingCharges * volume;
			responseMessage = "Volume is less than 2500 cm3 For " + RulesNameConstant.MEDIUM_PARCEL
					+ ", Your Shipping Cost is: " + costOfShipment + " " + currencyUnit;
			shippingCostResponse.setCostOfShipment(costOfShipment);
			shippingCostResponse.setMessage(responseMessage);
			log.info(responseMessage);

		} else {
			shippingCharges = Double.parseDouble(environment.getProperty("largeParcleCostAsPerVolume"));
			costOfShipment = shippingCharges * volume;
			responseMessage = "Volume is for " + RulesNameConstant.LARGE_PARCEL + ", Your Shipping Cost is: "
					+ costOfShipment + " " + currencyUnit;
			shippingCostResponse.setCostOfShipment(costOfShipment);
			shippingCostResponse.setMessage(responseMessage);
			log.info(responseMessage);

		}

		shippingCostResponse = getDataFromVoucherApiService(inputParameters,shippingCostResponse);

		return shippingCostResponse;

	}

	public ShippingCostResponse getDataFromVoucherApiService(InputParam inputParameters,ShippingCostResponse shippingCostResponse) {
		log.info("Function Called: getDataFromVoucherApiService");
		
//		if weight exceed from 50KG so we can't entertain and also not check about voucher as well as
		if (inputParameters.getWeight() < 50) {
			VoucherApiResponse voucherApiResponse = voucherApiService.getDataFromVoucherApiServie(inputParameters);

			shippingCostResponse.setCode(voucherApiResponse.getCode());
			shippingCostResponse.setDiscount(voucherApiResponse.getDiscount());
			shippingCostResponse.setExpiry(voucherApiResponse.getExpiry());
			shippingCostResponse.setError(voucherApiResponse.getError()); 
			shippingCostResponse.setVouchorExpired(voucherApiResponse.isVouchorExpired()); 
			
//			Customer is eligible for discount or not
			shippingCostResponse = customerDiscount.isCustomerAvailDiscount(shippingCostResponse);

		} else {
			shippingCostResponse.setMessage(responseMessage);
		}

		return shippingCostResponse;
	}

}
