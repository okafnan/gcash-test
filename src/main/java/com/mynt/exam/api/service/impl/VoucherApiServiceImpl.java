package com.mynt.exam.api.service.impl;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.mynt.exam.api.model.InputParam;
import com.mynt.exam.api.response.ShippingCostResponse;
import com.mynt.exam.api.response.VoucherApiResponse;
import com.mynt.exam.api.service.IsVoucherCodeExpired;
import com.mynt.exam.api.service.VoucherApiService;

@Service
public class VoucherApiServiceImpl implements VoucherApiService {

	private final static Log log = LogFactory.getLog(VoucherApiServiceImpl.class.getName());

	@Autowired
	private Environment environment;
	
	@Autowired
	private IsVoucherCodeExpired isVoucherCodeExpired;

//	Get date from external voucher API service with help of voucherCode and key 
	@Override 
	public VoucherApiResponse getDataFromVoucherApiServie(InputParam inputParameters) {
		log.info("Function Called: getDataFromVoucherApiServie");
		String getResponseFromVoucherApi = environment.getProperty("voucherCodeServiceURL".trim());

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		UriComponentsBuilder componentsBuilder = UriComponentsBuilder
				.fromHttpUrl(getResponseFromVoucherApi + inputParameters.getVoucherCode())
				.queryParam("key", inputParameters.getKey());
		VoucherApiResponse voucherApiResponse = new VoucherApiResponse();
		log.info("External Call for Voucher API: " + componentsBuilder.toUriString());
		try {

			voucherApiResponse = restTemplate.exchange(componentsBuilder.toUriString(), HttpMethod.GET,
					new HttpEntity<Object>(headers), VoucherApiResponse.class).getBody();
		} catch (HttpClientErrorException ex) {

//			We are getting exception because of invalid code response, 
//			This is simple way to extract error value using substring because this String not JSON

			String detailMessageString = ex.getMessage();
			detailMessageString = detailMessageString.substring(35, 47);
			voucherApiResponse.setCode(inputParameters.getVoucherCode());
			voucherApiResponse.setError(detailMessageString);
			return voucherApiResponse;

		}

//		Check customer voucher code is expired or not
		boolean isVoucherExpired=isVoucherCodeExpired.isVoucherCodeExpiredOrNot(voucherApiResponse);
		voucherApiResponse.setVouchorExpired(isVoucherExpired); 

		return voucherApiResponse;

	}

}
